# I. Manipulation du conteneur

* Liste des conteneur actif
____________________________________________________________________________________________
[noemie@localhost ~]$ sudo docker container ls -a
```
CONTAINER ID        IMAGE               COMMAND             CREATED              STATUS                          PORTS               NAMES

fb7b69ecd07d        alpine              "/bin/sh"           About a minute ago   Exited (0) About a minute ago                       quirky_yalow           
```

[noemie@localhost ~]$ sudo docker container ls
```
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

00cb37cc8159        alpine              "sleep 9999"        12 seconds ago      Up 11 seconds                           quirky_chatelet
```
* Conteneur lancé sur le `sleep`:

    Son nom est quirky_chatelet et son ID est 00cb37cc8159
____________________________________________________________________________________________

* 🌞Mise en évidence d'une partie de l'isolation mise en place par le conteneur
____________________________________________________________________________________________
[noemie@localhost ~]$ ps  -efxa
```
9174 ?        Ssl    0:01 /usr/bin/containerd

9837 ?        Sl     0:00  \_ containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/00cb37cc8159d5887b37f663132b29413e7c646f52dc065835edd73c54466243 -address /run/containerd/containerd.sock -co

9854 ?        Ss     0:00      \_ sleep 9999

9175 ?        Ssl    0:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/
containerd.sock
```
 ___________________________________________________________________________________________
    
[noemie@localhost ~]$ sudo docker exec -it 00cb37cc8159 sh
```
[sudo] Mot de passe de noemie : 

/ # ps -efa

PID   USER     TIME  COMMAND

1 root      0:00 sleep 9999

11 root      0:00 sh

20 root      0:00 ps -efa
```
____________________________________________________________________________________________
/ # ip a
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000

link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00

inet 127.0.0.1/8 scope host lo

    valid_lft forever preferred_lft forever

9: eth0@if10: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP

link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff

inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0

    valid_lft forever preferred_lft forever
```
____________________________________________________________________________________________

[noemie@localhost ~]$ ip a
```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000

link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00

inet 127.0.0.1/8 scope host lo

    valid_lft forever preferred_lft forever

inet6 ::1/128 scope host

    valid_lft forever preferred_lft forever

2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000

link/ether 08:00:27:7b:23:d2 brd ff:ff:ff:ff:ff:ff
inet 10.0.2.13/24 brd 10.0.2.255 scope global noprefixroute dynamic enp0s3
    valid_lft 85444sec preferred_lft 85444sec 
inet6 fe80::1c13:eee7:e4b4:a2af/64 scope link noprefixroute
    valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000

link/ether 08:00:27:53:8e:d1 brd ff:ff:ff:ff:ff:ff
inet 192.168.124.4/24 brd 192.168.124.255 scope global noprefixroute dynamic enp0s8
    valid_lft 898sec preferred_lft 898sec
inet6 fe80::2731:e8c7:1f37:269b/64 scope link noprefixroute
    valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default

link/ether 02:42:bb:3f:23:c7 brd ff:ff:ff:ff:ff:ff
inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    valid_lft forever preferred_lft forever
inet6 fe80::42:bbff:fe3f:23c7/64 scope link
    valid_lft forever preferred_lft forever
10: veth666cb02@if9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default

link/ether d2:22:d7:84:f2:38 brd ff:ff:ff:ff:ff:ff link-netnsid 0
inet6 fe80::d022:d7ff:fe84:f238/64 scope link
    valid_lft forever preferred_lft forever
```
___________________________________________________________________________________________
[noemie@localhost ~]$ cat /etc/passwd | awk -F: '{print $ 1}'
```
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
operator
games
ftp
nobody
systemd-network
dbus
polkitd
sshd
postfix
chrony
noemie
```
___________________________________________________________________________________________

/ #   cat /etc/passwd | awk -F: '{print $ 1}'
```
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
news
uucp
operator
man
postmaster
cron
ftp
sshd
at
squid
xfs
games
postgres
cyrus
vpopmail
ntp
smmsp
guest
nobody
```
__________________________________________________________________________________________
[noemie@localhost ~]$ df -h
```
Sys. de fichiers         Taille Utilisé Dispo Uti% Monté sur
devtmpfs                  1,9G       0  1,9G   0% /dev
tmpfs                     1,9G       0  1,9G   0% /dev/shm
tmpfs                     1,9G    8,7M  1,9G   1% /run
tmpfs                     1,9G       0  1,9G   0% /sys/fs/cgroup
/dev/mapper/centos-root    17G    1,9G   16G  11% /
/dev/sda1                1014M    174M  841M  18% /boot
tmpfs                     379M       0  379M   0% /run/user/1000
tmpfs                     379M       0  379M   0% /run/user/0
```
__________________________________________________________________________________________
/ # df -h
```
Filesystem                Size      Used Available Use% Mounted on
overlay                  17.0G      1.9G     15.1G  11% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                     1.8G         0      1.8G   0% /sys/fs/cgroup
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/mapper/centos-root
                        17.0G      1.9G     15.1G  11% /etc/resolv.conf
/dev/mapper/centos-root
                        17.0G      1.9G     15.1G  11% /etc/hostname
/dev/mapper/centos-root
                        17.0G      1.9G     15.1G  11% /etc/hosts
tmpfs                     1.8G         0      1.8G   0% /proc/asound
tmpfs                     1.8G         0      1.8G   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/timer_stats
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                     1.8G         0      1.8G   0% /proc/scsi
tmpfs                     1.8G         0      1.8G   0% /sys/firmware
```
__________________________________________________________________________________________
* 🌞 détruire le conteneur avec `docker rm`

[noemie@localhost ~]$ docker ps
```
CONTAINER ID      IMAGE      COMMAND        CREATED              STATUS          PORTS          NAMES
e65d0b309ae2      alpine     "sleep 9999"   About a minute ago   Up About a minute              lucid_gould
```
[noemie@localhost ~]$ docker rm -f e6d0b309ae2
```
e65d0b309ae2
```
`"rm -f" car le processus est toujour en cours d'execution donc on force son extiction`

[noemie@localhost ~]$ docker ps
```
CONTAINER ID      IMAGE      COMMAND        CREATED              STATUS          PORTS          NAMES
```
`Le processus c'est bien arrété`
__________________________________________________________________________________________
## 🌞 Lancer un conteneur NGINX
__________________________________________________________________________________________
         
[noemie@localhost ~]$ docker run -p 80 -d nginx
```
91ed57cd8752d5bed08e09752fdbff27d22aa7e4a9e88e99900118273d4c7ea3
```
`"-p 80" pour partager un port de l'hôte vers le port 80 du conteneur`

`"-d nginx" pour lancer le conteneur nginx en deamon `

[noemie@localhost ~]$ docker ps
```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                NAMES
212a91a292d8        nginx               "nginx -g 'daemon of…"   4 seconds ago       Up 3 seconds        0.0.0.0:80->80/tcp   infallible_matsumoto
```
`On voit bien ici quel port 80 du contener est forwarder vers le port 80 de l'hote`

[noemie@localhost ~]$ curl localhost
```
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
    width: 35em;
    margin: 0 auto;
    font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
`On peux voir avec la commande "curl localhost" qu'on récupère bien la page d'acceuil nginx partager sur le port 80`
_______________________________________________________________________________
[noemie@localhost ~]$ docker pull httpd:2.2
```
2.2: Pulling from library/httpd

f49cf87b52c1: Pull complete                                                                                       24b1e09cbcb7: Pull complete                                                                                       8a4e0d64e915: Pull complete                                                                                       bcbe0eb4ca51: Pull complete                                                                                       16e370c15d38: Pull complete                                                                                       Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
Status: Downloaded newer image for httpd:2.2
docker.io/library/httpd:2.2
```
`Cette commande nous permets de récupéré la version 2.2 de Apache`
__________________________________________________________________________________________
[noemie@localhost ~]$ docker image ls
```
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
nginx               latest              f7bb5701a33c        6 days ago          126MB
httpd               latest              c2aa7e16edd8        6 days ago          165MB
alpine              latest              965ea09ff2eb        2 months ago        5.55MB
httpd               2.2                 e06c3dbbfe23        23 months ago       171MB
```
`"httpd               2.2                 e06c3dbbfe23        23 months ago       171MB" cette ligne nous montre que nous avons bien récupéré l'image de Apache 2.2`
__________________________________________________________________________________________
   
[noemie@localhost ~]$ docker run -p 80:80 -d httpd
```
98292a6a864646892929f52edf0a5873d373e8deb93a29acec19cc998a3c2e34
```
`On execute le serveur Apache en deamon tout en partangeant le port 80`

[noemie@localhost ~]$ docker ps
```
CONTAINER ID        IMAGE               COMMAND              CREATED             STATUS              PORTS                NAMES
edefc4c2ef8f        httpd               "httpd-foreground"   4 minutes ago       Up 4 minutes        0.0.0.0:80->80/tcp   sharp_driscoll
```
   
[noemie@localhost ~]$ curl localhost
```
<html><body><h1>It works!</h1></body></html>
```
`On récupère bien le page du serveur apache sur le port 80`

__________________________________________________________________________________________

## Création d'image

[noemie@localhost dockerfile_directory]$ cat dockerfile
```
FROM alpine:latest
RUN apk add python3 && apk add curl
EXPOSE 8080
COPY . /app
WORKDIR /app
CMD python3 -m http.server 8888
```  
[noemie@localhost dockerfile_directory]$ docker build -t dockerfile  .
```
Sending build context to Docker daemon  2.048kB
Step 1/6 : FROM alpine:latest
---> 965ea09ff2eb
Step 2/6 : RUN apk add python3 && apk add curl
---> Running in 8ee4892dde5c
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/community/x86_64/APKINDEX.tar.gz
(1/11) Installing libbz2 (1.0.6-r7)
(2/11) Installing expat (2.2.8-r0)
(3/11) Installing libffi (3.2.1-r6)
(4/11) Installing gdbm (1.13-r1)
(5/11) Installing xz-libs (5.2.4-r0)
(6/11) Installing ncurses-terminfo-base (6.1_p20190518-r0)
(7/11) Installing ncurses-terminfo (6.1_p20190518-r0)
(8/11) Installing ncurses-libs (6.1_p20190518-r0)
(9/11) Installing readline (8.0.0-r0)
(10/11) Installing sqlite-libs (3.28.0-r2)
(11/11) Installing python3 (3.7.5-r1)
Executing busybox-1.30.1-r2.trigger
OK: 70 MiB in 25 packages
(1/4) Installing ca-certificates (20190108-r0)
(2/4) Installing nghttp2-libs (1.39.2-r0)
(3/4) Installing libcurl (7.66.0-r0)
(4/4) Installing curl (7.66.0-r0)
Executing busybox-1.30.1-r2.trigger
Executing ca-certificates-20190108-r0.trigger
OK: 71 MiB in 29 packages
Removing intermediate container 8ee4892dde5c
---> 163d4bb2a100
Step 3/6 : EXPOSE 8080
---> Running in ed0f2a7c2304
Removing intermediate container ed0f2a7c2304
---> 17f50a117187
Step 4/6 : COPY . /app
---> 8ee454e59a83
Step 5/6 : WORKDIR /app
---> Running in d194e26347e7
Removing intermediate container d194e26347e7
---> 6f71bb6c497f
Step 6/6 : CMD python3 -m http.server 8888
---> Running in e7bfb254f8d2
Removing intermediate container e7bfb254f8d2
---> c079ebcc517a
Successfully built c079ebcc517a
Successfully tagged dockerfile:latest
```
[noemie@localhost dockerfile_directory]$ docker run -p 8888:8888 -d dockerfile
```
fd10bd62327ed758012a20c5510841f23c6fec96defeaaf99c2759d10cb562d0
```
[noemie@localhost dockerfile_directory]$ docker ps
```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                              NAMES
fd10bd62327e        dockerfile          "/bin/sh -c 'python3…"   27 seconds ago      Up 26 seconds       8080/tcp, 0.0.0.0:8888->8888/tcp   fervent_diffie
```
[noemie@localhost dockerfile_directory]$ curl localhost:8888
```
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="dockerfile">dockerfile</a></li>
</ul>
<hr>
</body>
</html>
```
[noemie@localhost dockerfile_directory]$ docker run -v /home/noemie/dossier_partage/:/home/WORKDIR -d dockerfile

```
3ca511425b912641125b90b5c329417ba1f82d71e67273345dda49888c03bd39
```

# II. `docker-compose`


[noemie@localhost docker-compose_directory]$ cat docker-compose-v1.yml
```
version: "3.7" services:
pyweb:
    build:
    context: .
    dockerfile: Dockerfile
    ports:
    - "8888:8888"
    networks:
    - overlay networks:
overlay:
```
[noemie@localhost docker-compose_directory]$ cat docker-compose-v2.yml

```
version: "3.7" services:
web:
    build:
    context: ./dkr
    dockerfile: Dockerfile
    ports:
    - "8888:8888"
    networks:
    - overlay
server:
    image: nginx
    volumes:
    - ./nginx/conf/:/etc/nginx/conf.d/
    - ./nginx/certs:/certs
    ports:
    - "443:443"
     networks:
     - overlay networks:
overlay:
```
   
   