# TP2 docker BLANCHET Noémie

# I. Gestion de conteneurs Docker

* Processus liés à Docker :

```
[sloth@localhost ~]$ ps -ef | grep docker
root      1762     1  0 20:14 ?        00:00:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
root      1926  1761  0 20:14 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/a278113f651ee1cec51554009865972b2b74c9125cf0dd2bb10c3763bd513d90 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
sloth       2148  1719  0 20:20 pts/0    00:00:00 grep --color=auto docker
```

* API HTTP dockerd :


```
[sloth@localhost ~]$ curl -X GET --unix-socket /var/run/docker.sock http:/containers/json
[{"Id":"a278113f651ee1cec51554009865972b2b74c9125cf0dd2bb10c3763bd513d90","Names":["/friendly_shtern"],"Image":"alpine","ImageID":"sha256:e7d92cdc71feacf90708cb59182d0df1b911f8ae022d29e8e95d75ca6a99776a","Command":"sleep 99999","Created":1580670852,"Ports":[],"Labels":{},"State":"running","Status":"Up 3 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"0bb3945a9220135fdf8544bc4d615784ddabf7407d54ee8a675f1142ad4b45bd","EndpointID":"661a03aab3722364013f8d567ffd376bccf2f800c112289dc43c7fc8e41ab8f7","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}]
```

## 1. Namespaces

### A. Exploration manuelle

* Namespaces

```
[sloth@localhost ~]$ lsns -l
        NS TYPE  NPROCS   PID USER COMMAND
4026531836 pid        3  1579 sloth  -bash
4026531837 user       3  1579 sloth  -bash
4026531838 uts        3  1579 sloth  -bash
4026531839 ipc        3  1579 sloth  -bash
4026531840 mnt        3  1579 sloth  -bash
4026531956 net        3  1579 sloth  -bash
```

### B. `unshare`

* Pseudo-conteneur avec `unshare`

```
[sloth@15 ~]$ unshare -n -m -p -u -f
[sloth@15 ~]$ sudo !!
[root@15 sloth]# lsns -l
	NS TYPE  NPROCS   PID USER   COMMAND
4026531836 pid      100     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531837 user     103     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531838 uts       99     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531839 ipc      102     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531840 mnt       95     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026531856 mnt        1    13 root   kdevtmpfs
4026531956 net       99     1 root   /usr/lib/systemd/systemd --switched-root --system --deserialize 22
4026532121 mnt        2   739 root   /usr/sbin/NetworkManager --no-daemon
4026532170 mnt        1   713 chrony /usr/sbin/chronyd
4026532177 mnt        1  1941 root   sleep 99999
4026532178 uts        1  1941 root   sleep 99999
4026532179 ipc        1  1941 root   sleep 99999
4026532180 pid        1  1941 root   sleep 99999
4026532182 net        1  1941 root   sleep 99999
4026532258 mnt        3  2180 root   unshare -n -m -p -u -f
4026532259 uts        3  2180 root   unshare -n -m -p -u -f
4026532260 pid        2  2181 root   -bash
4026532262 net        3  2180 root   unshare -n -m -p -u -f
```
### C. Avec docker

* Dans quels namespaces il s'execute

```
[sloth@localhost ~]$ docker run -d debian sleep 99999
5acf50dad01c90b8d902668b1e6bfc57c7a9b922002109a1f8d046bd9a8088c4
[sloth@localhost ~]$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
5acf50dad01c        debian              "sleep 99999"       8 seconds ago       Up 7 seconds                            vibrant_volhard
[sloth@localhost ~]$  PID=$(docker inspect -f {{.State.Pid}} 5acf50dad01c)
[sloth@localhost ~]$ echo $PID
2308
[sloth@localhost ~]$
[sloth@localhost ~]$
[sloth@localhost ~]$ sudo ls -al /proc/2308/ns
total 0
dr-x--x--x. 2 root root 0  2 févr. 21:02 .
dr-xr-xr-x. 9 root root 0  2 févr. 21:02 ..
lrwxrwxrwx. 1 root root 0  2 févr. 21:04 ipc -> ipc:[4026532179]
lrwxrwxrwx. 1 root root 0  2 févr. 21:04 mnt -> mnt:[4026532177]
lrwxrwxrwx. 1 root root 0  2 févr. 21:02 net -> net:[4026532182]
lrwxrwxrwx. 1 root root 0  2 févr. 21:04 pid -> pid:[4026532180]
lrwxrwxrwx. 1 root root 0  2 févr. 21:04 user -> user:[4026531837]
lrwxrwxrwx. 1 root root 0  2 févr. 21:04 uts -> uts:[4026532178]
```

### D. `nsenter`

* `nsenter` pour rentrer dans les namespaces avec le shell

```
[sloth@localhost ~]$ sudo nsenter -t 2308 -i -m -n -p -u
mesg: ttyname failed: No such device
root@5acf50dad01c:/# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```
```
root@5acf50dad01c:/# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 22:02 ?        00:00:00 sleep 99999
root        30     0  0 22:23 ?        00:00:00 -bash
root       354    30  0 22:27 ?        00:00:00 ps -ef
```
```
root@5acf50dad01c:/# df -aTh
Filesystem              Type     Size  Used Avail Use% Mounted on
overlay                 overlay   17G  2.5G   15G  15% /
proc                    proc        0     0     0    - /proc
tmpfs                   tmpfs     64M     0   64M   0% /dev
devpts                  devpts      0     0     0    - /dev/pts
sysfs                   sysfs       0     0     0    - /sys
tmpfs                   tmpfs    1.9G     0  1.9G   0% /sys/fs/cgroup
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/systemd
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/devices
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/cpu,cpuacct
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/freezer
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/cpuset
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/hugetlb
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/net_cls,net_prio
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/perf_event
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/blkio
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/pids
cgroup                  cgroup      0     0     0    - /sys/fs/cgroup/memory
mqueue                  mqueue      0     0     0    - /dev/mqueue
shm                     tmpfs     64M     0   64M   0% /dev/shm
/dev/mapper/centos-root xfs       17G  2.5G   15G  15% /etc/resolv.conf
/dev/mapper/centos-root xfs       17G  2.5G   15G  15% /etc/hostname
/dev/mapper/centos-root xfs       17G  2.5G   15G  15% /etc/hosts
proc                    proc        0     0     0    - /proc/bus
proc                    proc        0     0     0    - /proc/fs
proc                    proc        0     0     0    - /proc/irq
proc                    proc        0     0     0    - /proc/sys
proc                    proc        0     0     0    - /proc/sysrq-trigger
tmpfs                   tmpfs    1.9G     0  1.9G   0% /proc/asound
tmpfs                   tmpfs    1.9G     0  1.9G   0% /proc/acpi
tmpfs                   tmpfs     64M     0   64M   0% /proc/kcore
tmpfs                   tmpfs     64M     0   64M   0% /proc/keys
tmpfs                   tmpfs     64M     0   64M   0% /proc/timer_list
tmpfs                   tmpfs     64M     0   64M   0% /proc/timer_stats
tmpfs                   tmpfs     64M     0   64M   0% /proc/sched_debug
tmpfs                   tmpfs    1.9G     0  1.9G   0% /proc/scsi
tmpfs                   tmpfs    1.9G     0  1.9G   0% /sys/firmware
```
### E. Et alors, les namespaces User ?

* Config' pour que Docker utilise les namespaces de type User

```
[sloth@localhost ~]$
[sloth@localhost ~]$
```

``On me l'a expliqué mais j'ai pas réussis à le refaire``


### F. Isolation réseau ? 

* Lancer un conteneur tout simple

```
[sloth@localhost ~]$ sudo docker run -d -p 8888:7777 debian sleep 99999
[sudo] Mot de passe de sloth : 
5ae471bbd4b6deb8e34ea63c594909d0defb631590240c0eea6fa2aa5dc6aa87
[sloth@localhost ~]$ docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                    NAMES
5ae471bbd4b6        debian              "sleep 99999"       19 seconds ago      Up 18 seconds       0.0.0.0:8888->7777/tcp   objective_wilson
```

* Vérifier le réseau du conteneur et de l'hôte
```
[sloth@localhost ~]$ sudo docker exec -ti 5ae471bbd4b6 /bin/bash
root@5ae471bbd4b6:/# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
9: eth0@if10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:acff:fe11:2/64 scope link
       valid_lft forever preferred_lft forever
4: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default
    link/ether 02:42:0f:e6:1a:11 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:fff:fee6:1a11/64 scope link
       valid_lft forever preferred_lft forever
6: veth09e429f@if04: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default
    link/ether 9a:4f:5c:c3:6a:d6 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::984f:5cff:fec3:6ad6/64 scope link
       valid_lft forever preferred_lft forever
```
  
* identifier les règles *iptables* liées à la création de votre conteneur  

```
[sloth@localhost ~]$ sudo iptables -vnL
[...]
Chain DOCKER (1 references)
 pkts bytes target     prot opt in     out     source               destination
    0     0 ACCEPT     tcp  --  !docker0 docker0  0.0.0.0/0            172.17.0.2           tcp dpt:7777
[...]
```

## 2. Cgroups

### A. Découverte manuelle

* Lancer un conteneur Docker et déduire dans quel cgroup il s'exécute
```
[sloth@localhost ~]$ sudo docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
4799d935b553        debian              "sleep 99999"       13 seconds ago      Up 12 seconds                           frosty_nightingale
[sloth@localhost ~]$ docker top 4799d935b553
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                2847                2831                0                   09:56               ?                   00:00:00            sleep 99999
[sloth@localhost ~]$ cat /proc/2847/cgroup
11:pids:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
10:blkio:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
9:devices:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
8:freezer:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
7:hugetlb:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
6:net_prio,net_cls:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
5:perf_event:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
4:cpuacct,cpu:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
3:memory:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
2:cpuset:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
1:name=systemd:/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86
```

### B. Utilisation par Docker

* La mémoire RAM max autorisé dans un conteneur Docker
```
[sloth@localhost ~]$ cat /sys/fs/cgroup/memory/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86/
memory.kmem.max_usage_in_bytes
8724480
```
* le nombre de processus max dans un conteneur Docker
```
[sloth@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86/cpu.shares
10952
```
* explorer les possibilités avec des cgroups  

* Changer les valeurs cgroups par défaut avec des options avec `docker run` : (--cpu-shares ; memory ; --cpu-period)
```
[sloth@localhost ~]$ docker run  --cpu-shares=1024 --memory=0 --cpu-period="100000" -d debian sleep 99999
7303a6262807493b3ff388c6e7712f6e25cb27c1bd96cbcd7e0b9499b03a3d7d
```
* préciser les options utilisées

* Définition reprise du `man`:

```
--cpu-shares=1024 :
Définissez cet indicateur sur une valeur supérieure ou inférieure à la valeur par défaut de 1024 pour augmenter ou réduire le poids du conteneur et lui donner accès à une proportion plus ou moins grande des cycles de processeur de la machine hôte.

--memory=0 :
Quantité maximale de mémoire que le conteneur peut utiliser

--cpu-period="100000" :
Spécifiez la période du planificateur CPU CFS, qui est utilisée avec --cpu-quota. Par défaut, 100 micro-secondes.
```
* prouver en regardant dans `/sys` qu'elles sont utilisées
```
--cpu-shares=1024 :
[sloth@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86/cpu.shares
1024

--memory=0 :
[sloth@localhost ~]$ cat /sys/fs/cgroup/memory/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86/memory.kmem.max_usage_in_bytes
0

--cpu-period="100000" :
[sloth@localhost ~]$ cat /sys/fs/cgroup/cpu/docker/4799d935b5534040f98ad29e236d9957b70301abc6aa3604933ebd9265721b86/cpu.cfs_period_us
100000
```
## 3. Capabilities

### A. Découverte manuelle

* Déterminer les capabilities actuellement utilisées par votre shell
```
[sloth@localhost ~]$ cat /proc/1/status | grep Cap
CapInh: 0000000000000000
CapPrm: 0000001fffffffff
CapEff: 0000001fffffffff
CapBnd: 0000001fffffffff
CapAmb: 0000000000000000
[sloth@localhost ~]$ capsh --decode=0000001fffffffff
0x0000001fffffffff=cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,35,36
```

* Déterminer les capabilities du processus lancé par un conteneur Docker

```
[sloth@localhost ~]$ cat /proc/3218/status | grep Cap
CapInh: 00000000a80425fb
CapPrm: 00000000a80425fb
CapEff: 00000000a80425fb
CapBnd: 00000000a80425fb
CapAmb: 0000000000000000
[sloth@localhost ~]$ capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
```

* avec `/proc`

```
[sloth@localhost ~]$ cat /proc/3218/status
Name:   sleep
Umask:  0022
State:  S (sleeping)
Tgid:   3218
Ngid:   0
Pid:    3218
PPid:   3201
TracerPid:      0
Uid:    0       0       0       0
Gid:    0       0       0       0
FDSize: 64
Groups:
VmPeak:     2284 kB
VmSize:     2284 kB
VmLck:         0 kB
VmPin:         0 kB
VmHWM:       376 kB
VmRSS:       376 kB
RssAnon:              60 kB
RssFile:             316 kB
RssShmem:              0 kB
VmData:      168 kB
VmStk:       132 kB
VmExe:        24 kB
VmLib:      1424 kB
VmPTE:        20 kB
VmSwap:        0 kB
Threads:        1
SigQ:   0/7261
SigPnd: 0000000000000000
ShdPnd: 0000000000000000
SigBlk: 0000000000000000
SigIgn: 0000000000000000
SigCgt: 0000000000000000
CapInh: 00000000a80425fb
CapPrm: 00000000a80425fb
CapEff: 00000000a80425fb
CapBnd: 00000000a80425fb
CapAmb: 0000000000000000
NoNewPrivs:     0
Seccomp:        2
Speculation_Store_Bypass:       vulnerable
Cpus_allowed:   1
Cpus_allowed_list:      0
Mems_allowed:   00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000000,00000001
Mems_allowed_list:      0
voluntary_ctxt_switches:        43
nonvoluntary_ctxt_switches:     6
```


* Jouer avec `ping`
* trouver le chemin absolu de `ping`

```
/bin/ping
```

* récupérer la liste de ses capabilities

```
[sloth@localhost ~]$ getcap /bin/ping
/bin/ping = cap_net_admin,cap_net_raw+p
```

* enlever toutes les capabilities
  * en utilisant une liste vide
  
  ```
  sudo setcap '' /bin/ping
  ``` 

* vérifier que `ping` ne fonctionne plus

```
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 0 received, 100% packet loss, time 2503ms
```

* vérifier avec `strace` que c'est bien l'accès à l'ICMP qui a été enlevé

```
strace ping 8.8.8.8
socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
```

### B. Utilisation par Docker

* lancer un conteneur NGINX avec le min de capabilities pour fonctionner

```
[sloth@localhost ~]$ capsh --decode=00000000a80425fb
0x00000000a80425fb=cap_chown,cap_dac_override,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_net_bind_service,cap_net_raw,cap_sys_chroot,cap_mknod,cap_audit_write,cap_setfcap
[sloth@localhost ~]$ docker run --cap-drop all --cap-add cap_chown --cap-add cap_dac_override --cap-add cap_fowner --cap-add cap_fsetid --cap-add cap_kill --cap-add cap_setgid --cap-add cap_setuid --cap-add cap_setpcap --cap-add cap_net_bind_service --cap-add cap_net_raw --cap-add cap_sys_chroot --cap-add cap_mknod --cap-add cap_audit_write --cap-add cap_setfcap -d -p 80:80 nginx
3e8cedfdf3619fd0e422cd38c82afb8b3b300c4be10c66fefe571c19a21d44d3
```

* prouver qu'il fonctionne

```
[sloth@localhost ~]$ curl localhost
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

* expliquer toutes les capabilities dont il a besoin

* copier/coller du `man`

```
cap_chown :
  Apportez des modifications arbitraires aux fichiers UID et GID

cap_dac_override :
  Contournez le fichier pour lire, écrire et exécuter des vérifications d'autorisations.

cap_fowner :
  * Contourner les vérifications d'autorisation sur les opérations qui nécessitent normalement
    l'UID du système de fichiers du processus pour correspondre à l'UID du
    fichier (par exemple, chmod  , utime  ), à l'exclusion de ces opérations
    couvert par CAP_DAC_OVERRIDE et CAP_DAC_READ_SEARCH ;
  * définir des drapeaux d'inode (voir ioctl_iflags  ) sur des fichiers arbitraires;
  * définir des listes de contrôle d'accès (ACL) sur des fichiers arbitraires;
  * ignorer le bit collant de répertoire lors de la suppression de fichiers;
  * modifier les attributs étendus de l' utilisateur sur le répertoire collant appartenant à tout utilisateur;
  * spécifiez O_NOATIME pour les fichiers arbitraires dans open  et fcntl  .

cap_fsetid :
  * Ne pas effacer les bits du mode set-user-ID et set-group-ID lorsqu'un le fichier est modifié;
  * définir le bit set-group-ID pour un fichier dont le GID ne correspond pas
    le système de fichiers ou l'un des GID supplémentaires du processus d'appel.

cap_kill :
  Contourner les contrôles de permission pour envoyer des signaux.
  Cela inclut l'utilisation de l'opération ioctl KDSIGACCEPT 

cap_setgid :
  * Effectuer des manipulations arbitraires des GID de processus et liste GID supplémentaire;
  * forger GID lors du passage des informations d'identification de socket via le domaine UNIX prises;
  * écrire un mappage d'ID de groupe dans un espace de noms d'utilisateur

cap_setuid :
  * Effectuer des manipulations arbitraires des UID de processus;
  * forger l'UID lors du passage des informations d'identification de socket via le domaine UNIX prises;
  * écrire un mappage d'ID utilisateur dans un espace de noms utilisateur

cap_setpcap :
  Si les capacités de fichiers sont prises en charge (c'est-à-dire depuis Linux 2.6.24):
  ajouter toute capacité de l'ensemble de délimitation du thread appelant à
  son ensemble héritable;  supprimer des capacités de l'ensemble de délimitation
  ;  apporter des modifications aux bits sécurisés drapeaux.

  Si les capacités de fichier ne sont pas prises en charge (c'est-à-dire, les noyaux avant
  Linux 2.6.24): accordez ou supprimez toute capacité dans l'appelant
  capacité autorisée définie vers ou à partir de tout autre processus.  (Cette
  La propriété de CAP_SETPCAP n'est pas disponible lorsque le noyau est
  configuré pour prendre en charge les capacités de fichier, car CAP_SETPCAP a
  sémantique entièrement différente pour ces noyaux.)

cap_net_bind_service :
  Lier un socket aux ports privilégiés du domaine Internet (port
  nombres inférieurs à 1024).

cap_net_raw :
  * Utilisez des prises RAW et PACKET;
  * lier à n'importe quelle adresse pour un proxy transparent.

cap_sys_chroot :
  * Utilisez chroot;
  * changer les espaces de noms de montage en utilisant setns

cap_mknod :
  Créez des fichiers spéciaux à l'aide de mknod

cap_audit_write :
  Écrivez les enregistrements dans le journal d'audit du noyau.

cap_setfcap :
  Définissez des capacités arbitraires sur un fichier.
```
